package ffufm.kenneth.api.spec.handler.user.project.implementation

import ffufm.kenneth.api.PassTestBase
import ffufm.kenneth.api.spec.handler.project.ProjectProjectDatabaseHandler
import ffufm.kenneth.api.spec.handler.user.utils.EntityGenerator
import kotlinx.coroutines.runBlocking
import org.junit.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.server.ResponseStatusException
import kotlin.test.assertEquals
import kotlin.test.assertFailsWith

class ProjectDatabaseHandlerTest: PassTestBase() {
    @Autowired
    private lateinit var projectProjectDatabaseHandler: ProjectProjectDatabaseHandler

    @Test
    fun `create should return project`() = runBlocking {
        val user = userUserRepository.save(EntityGenerator.createUser())
        val body = EntityGenerator.createProject().toDto()
        val createdProject = projectProjectDatabaseHandler.createProject(body, user.id!!)
        assertEquals(1, projectProjectRepository.findAll().count())
        assertEquals(body.name, createdProject.name)
        assertEquals(body.description, createdProject.description)
        assertEquals(body.status, createdProject.status)
    }

    @Test
    fun `create should fail given invalid userId`() = runBlocking {
        val body = EntityGenerator.createProject().toDto()
        val invalidId : Long  = 123

        val exception = assertFailsWith<ResponseStatusException> {
            projectProjectDatabaseHandler.createProject(body, invalidId)
        }
        val expectedException = "404 NOT_FOUND \"UserUser with ID $invalidId not found\""

        assertEquals(expectedException, exception.message)
    }

    @Test
    fun `update should return updated project data`() = runBlocking{
        val project = EntityGenerator.createProject()
        val original = projectProjectRepository.save(project)
        val body = original.copy(
            name = "CENTIE",
            description = "National University Engineering Website",
            status = "ON_GOING"
        )

        val updatedProject = projectProjectDatabaseHandler.update(
            body.toDto(), original.id!!
        )
        assertEquals(body.name, updatedProject.name)
        assertEquals(body.description, updatedProject.description)
        assertEquals(body.status, updatedProject.status)
    }

    @Test
    fun `delete project should return`() = runBlocking {
        val project = EntityGenerator.createProject()
        val createdProject = projectProjectRepository.save(project)
        assertEquals(1, projectProjectRepository.findAll().count())
        projectProjectDatabaseHandler.remove(createdProject.id!!)
        assertEquals(0,projectProjectRepository.findAll().count())
    }

    @Test
    fun `getAll projects return all projects`() = runBlocking {
        val owner = userUserRepository.save(EntityGenerator.createUser())
        val project =  EntityGenerator.createProject().copy( owner = owner )

        val maxResults: Int = 100
        val page: Int = 0

        projectProjectRepository.saveAll(
            listOf(
                project,
                project.copy( status = "COMPLETED")
            )
        )
        val users = projectProjectDatabaseHandler.getAll(maxResults,page)
        assertEquals(2, users.count())
    }

    @Test
    fun `updateProject should fail given invalid projectId`() = runBlocking {
        val owner = userUserRepository.save(EntityGenerator.createUser())
        val project = projectProjectRepository.save(
            EntityGenerator.createProject().copy( owner = owner ))

        val body = project.copy()
        val invalidId : Long  = 123

        val exception = assertFailsWith<ResponseStatusException> {
            projectProjectDatabaseHandler.update(body.toDto(), invalidId)
        }
        val expectedException = "404 NOT_FOUND \"Project with id $invalidId not exist\""

        assertEquals(expectedException, exception.message)
    }

}