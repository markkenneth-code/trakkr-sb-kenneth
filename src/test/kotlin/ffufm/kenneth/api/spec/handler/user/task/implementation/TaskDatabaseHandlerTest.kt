package ffufm.kenneth.api.spec.handler.user.task.implementation

import ffufm.kenneth.api.PassTestBase
import ffufm.kenneth.api.spec.handler.user.utils.EntityGenerator
import kotlinx.coroutines.runBlocking
import org.junit.Test
import org.springframework.beans.factory.annotation.Autowired
import kotlin.test.assertEquals
import ffufm.kenneth.api.spec.handler.task.TaskTaskDatabaseHandler
import org.springframework.web.server.ResponseStatusException
import kotlin.test.assertFailsWith


class TaskDatabaseHandlerTest: PassTestBase() {
    @Autowired
    private lateinit var taskDatabaseHandler: TaskTaskDatabaseHandler

    @Test
    fun `create should return task`() = runBlocking{
        assertEquals(0, taskTaskRepository.findAll().count())
        val owner = userUserRepository.save(EntityGenerator.createUser())
        val project = projectProjectRepository.save(
            EntityGenerator.createProject().copy( owner = owner ))


        val body = EntityGenerator.createTasks().toDto()
        val createdTask = taskDatabaseHandler.createTasks(body, project.id!!, owner.id!!)

        assertEquals(1, taskTaskRepository.findAll().count())
        assertEquals(body.name, createdTask.name)
        assertEquals(body.description, createdTask.description)
        assertEquals(body.status, createdTask.status)
    }

    @Test
    fun `create should fail given invalid userId`() = runBlocking {

        val owner = userUserRepository.save(EntityGenerator.createUser())
        val project = projectProjectRepository.save(
            EntityGenerator.createProject().copy( owner = owner ))

        val invalidId : Long  = 123
        val body = EntityGenerator.createTasks().toDto()

        val exception = assertFailsWith<ResponseStatusException> {
            taskDatabaseHandler.createTasks(body, project.id!!, invalidId )
        }
        val expectedException = "404 NOT_FOUND \"UserUser with ID $invalidId not found\""

        assertEquals(expectedException, exception.message)
    }

    @Test
    fun `create should fail given invalid projectId`() = runBlocking {

        val owner = userUserRepository.save(EntityGenerator.createUser())
        val project = projectProjectRepository.save(
            EntityGenerator.createProject().copy( owner = owner ))

        val invalidId : Long  = 123
        val body = EntityGenerator.createTasks().toDto()

        val exception = assertFailsWith<ResponseStatusException> {
            taskDatabaseHandler.createTasks(body, invalidId, owner.id!! )
        }
        val expectedException = "404 NOT_FOUND \"ProjectProject with ID $invalidId not found\""

        assertEquals(expectedException, exception.message)
    }

    @Test
    fun `update should return updated task`() = runBlocking {
        val task = EntityGenerator.createTasks()
        val original = taskTaskRepository.save(task)
        val body = original.copy(
            name = "Registration",
            description = "Connect to database",
            status = "ON-GOING"
        )

        val updatedTask = taskDatabaseHandler.update(
            body.toDto(), original.id!!
        )
        assertEquals(body.name, updatedTask.name)
        assertEquals(body.description, updatedTask.description)
        assertEquals(body.status, updatedTask.status)
    }


    @Test
    fun `delete task should return`() = runBlocking {
        val task = EntityGenerator.createTasks()
        val createdTask = taskTaskRepository.save(task)
        assertEquals(1, taskTaskRepository.findAll().count())
        taskDatabaseHandler.remove(createdTask.id!!)
        assertEquals(0, taskTaskRepository.findAll().count())
    }
}