package ffufm.kenneth.api.spec.handler.user.utils

import ffufm.kenneth.api.spec.dbo.project.ProjectProject
import ffufm.kenneth.api.spec.dbo.task.TaskTask
import ffufm.kenneth.api.spec.dbo.user.UserAddress
import ffufm.kenneth.api.spec.dbo.user.UserContactDetail
import ffufm.kenneth.api.spec.dbo.user.UserUser


object EntityGenerator {
    fun createUser(): UserUser = UserUser(
        firstName = "Brandon",
        lastName = "Cruz",
        email = "brandon@brandon.com",
        userType = "CR"
    )

    fun createContact(): UserContactDetail = UserContactDetail(
        contactDetail = "09093223500",
        contactType = "Home"
    )

    fun createAddress(): UserAddress = UserAddress(
        street = "HarmonyHills",
        barangay = "Muzon",
        city = "SJDM",
        province = "Bulacan",
        zipCode = "3023"
    )

    fun createProject(): ProjectProject = ProjectProject(
        name = "Foodernity",
        description = "Food sharing system",
        status = "ON_GOING"
    )

    fun createTasks(): TaskTask = TaskTask(
        name = "Login",
        description = "Apply validations",
        status = "ON-GOING"
    )
}