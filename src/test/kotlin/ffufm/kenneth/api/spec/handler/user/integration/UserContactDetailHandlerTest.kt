package ffufm.kenneth.api.spec.handler.user.integration

import com.fasterxml.jackson.databind.ObjectMapper
import ffufm.kenneth.api.PassTestBase
import ffufm.kenneth.api.repositories.user.UserContactDetailRepository
import ffufm.kenneth.api.spec.dbo.user.UserContactDetail
import ffufm.kenneth.api.spec.handler.user.utils.EntityGenerator
import org.hamcrest.CoreMatchers
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpHeaders
import org.springframework.http.MediaType
import org.springframework.security.test.context.support.WithMockUser
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.delete
import org.springframework.test.web.servlet.get
import org.springframework.test.web.servlet.post
import org.springframework.test.web.servlet.put

class UserContactDetailHandlerTest : PassTestBase() {

    @Autowired
    private lateinit var objectMapper: ObjectMapper

    @Autowired
    private lateinit var mockMvc: MockMvc

    @Test
    @WithMockUser
    fun `create contact details should return isOk`(){
        val user = userUserRepository.save(EntityGenerator.createUser())
        val body = EntityGenerator.createContact()
        mockMvc.post("/users/{id}/contact-details/", user.id!!){
            accept(MediaType.APPLICATION_JSON)
            contentType = MediaType.APPLICATION_JSON
            content = objectMapper.writeValueAsString(body)
        }.andExpect {
            status { isOk() }
        }
    }

//    @Before
//    @After
//    fun cleanRepositories() {
//        userContactDetailRepository.deleteAll()
//    }

//    @Test
//    @WithMockUser
//    fun `test createContactDetail`() {
//        val body: UserContactDetail = UserContactDetail()
//        val id: Long = 0
//                mockMvc.post("/users/{id}/contact-details/", id) {
//                    accept(MediaType.APPLICATION_JSON)
//                    contentType = MediaType.APPLICATION_JSON
//                    content = objectMapper.writeValueAsString(body)
//                }.andExpect {
//                    status { isOk }
//
//                }
//    }

//    @Test
//    @WithMockUser
//    fun `test remove`() {
//        val id: Long = 0
//                mockMvc.delete("/users/contact-details/{id}/", id) {
//                    accept(MediaType.APPLICATION_JSON)
//                    contentType = MediaType.APPLICATION_JSON
//
//                }.andExpect {
//                    status { isOk }
//
//                }
//    }

//    @Test
//    @WithMockUser
//    fun `test updateContactDetails`() {
//        val body: UserContactDetail = UserContactDetail()
//        val id: Long = 0
//                mockMvc.put("/users/contact-details/{id}/", id) {
//                    accept(MediaType.APPLICATION_JSON)
//                    contentType = MediaType.APPLICATION_JSON
//                    content = objectMapper.writeValueAsString(body)
//                }.andExpect {
//                    status { isOk }
//
//                }
//    }
}
