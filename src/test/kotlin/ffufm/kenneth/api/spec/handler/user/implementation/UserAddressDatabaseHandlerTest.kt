package ffufm.kenneth.api.spec.handler.user.implementation

import ffufm.kenneth.api.PassTestBase
import ffufm.kenneth.api.repositories.user.UserAddressRepository
import ffufm.kenneth.api.spec.dbo.user.UserAddress
import ffufm.kenneth.api.spec.handler.user.UserAddressDatabaseHandler
import kotlinx.coroutines.runBlocking
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.springframework.beans.factory.annotation.Autowired

class UserAddressDatabaseHandlerTest : PassTestBase() {


    @Autowired
    lateinit var userAddressDatabaseHandler: UserAddressDatabaseHandler

//    @Before
//    @After
//    fun cleanRepositories() {
//        userAddressRepository.deleteAll()
//    }

    @Test
    fun `test remove`() = runBlocking {
        val id: Long = 0
        userAddressDatabaseHandler.remove(id)
        Unit
    }

//    @Test
//    fun `test updateAddress`() = runBlocking {
//        val body: UserAddress = UserAddress()
//        val id: Long = 0
//        userAddressDatabaseHandler.updateAddress(body, id)
//        Unit
//    }

/*    @Test
    fun `test createAddress`() = runBlocking {
        val body: UserAddress = UserAddress()
        val id: Long = 0
        userAddressDatabaseHandler.createAddress(body, id)
        Unit
    }*/
}
