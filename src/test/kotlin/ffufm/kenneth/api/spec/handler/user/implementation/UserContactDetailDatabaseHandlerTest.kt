package ffufm.kenneth.api.spec.handler.user.implementation

import ffufm.kenneth.api.PassTestBase
import ffufm.kenneth.api.repositories.user.UserContactDetailRepository
import ffufm.kenneth.api.spec.dbo.user.UserContactDetail
import ffufm.kenneth.api.spec.handler.user.UserContactDetailDatabaseHandler
import ffufm.kenneth.api.spec.handler.user.utils.EntityGenerator
import kotlinx.coroutines.runBlocking
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.security.test.context.support.WithMockUser
import kotlin.test.assertEquals

class UserContactDetailDatabaseHandlerTest : PassTestBase() {


    @Autowired
    lateinit var userContactDetailDatabaseHandler: UserContactDetailDatabaseHandler


    @Test
    fun `create should return contactDetail`()= runBlocking {
        assertEquals(0, userContactDetailRepository.findAll().count())
        val user = EntityGenerator.createUser()
        val createdUser = userUserRepository.save(user)

        val contactDetail = EntityGenerator.createContact()
        val createdContactDetail = userContactDetailDatabaseHandler.createContactDetail(contactDetail.toDto(), createdUser.id!!)

        assertEquals(1, userContactDetailRepository.findAll().count())
        assertEquals(contactDetail.contactDetail, createdContactDetail.contactDetail)
        assertEquals(contactDetail.contactType, createdContactDetail.contactType)
    }

//    fun `delete contact should work`(){
//        val contact = EntityGenerator.createContact()
//        val createdContact = userContactDetailRepository.save(contact)
//
//        assertEquals(1, userContactDetailRepository.findAll().count())
//        userContactDetailDatabaseHandler.remove(createdContact.id!!)
//        assertEquals(0, userContactDetailRepository.findAll().count())
//    }

//    @Test
//    fun `test createContactDetail`() = runBlocking {
//        val body: UserContactDetail = UserContactDetail()
//        val id: Long = 0
//        userContactDetailDatabaseHandler.createContactDetail(body, id)
//        Unit
//    }

    @Test
    fun `test remove`() = runBlocking {
        val id: Long = 0
        userContactDetailDatabaseHandler.remove(id)
        Unit
    }

//    @Test
//    fun `test updateContactDetails`() = runBlocking {
//        val body: UserContactDetail = UserContactDetail()
//        val id: Long = 0
//        userContactDetailDatabaseHandler.updateContactDetails(body, id)
//        Unit
//    }
}
