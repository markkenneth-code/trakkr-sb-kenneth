package ffufm.kenneth.api.spec.handler.user.implementation

import ffufm.kenneth.api.PassTestBase
import ffufm.kenneth.api.repositories.user.UserUserRepository
import ffufm.kenneth.api.spec.dbo.user.UserUser
import ffufm.kenneth.api.spec.handler.user.UserUserDatabaseHandler
import ffufm.kenneth.api.spec.handler.user.utils.EntityGenerator
import kotlinx.coroutines.runBlocking
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.springframework.beans.factory.annotation.Autowired
import kotlin.test.assertEquals

class UserUserDatabaseHandlerTest : PassTestBase() {

    @Autowired
    lateinit var userUserDatabaseHandler: UserUserDatabaseHandler

//    @Before
//    @After
//    fun cleanRepositories() {
//        userUserRepository.deleteAll()
//    }

    @Test
    fun `getById should return a user`() = runBlocking {
        val user = UserUser(
            firstName = "Kenneth",
            lastName = "Tabjan",
            email = "markennethdelacruz@gmail.com",
            userType = "GC"
        )
        val savedUser = userUserRepository.save(user)

        userUserDatabaseHandler.getById(savedUser.id!!)

        assertEquals(userUserRepository.findAll().size, 1)
    }

    @Test
    fun `delete user should work`() = runBlocking{
        val user = EntityGenerator.createUser()
        val createdUser = userUserRepository.save(user)

        assertEquals(1, userUserRepository.findAll().count())
        userUserDatabaseHandler.remove(createdUser.id!!)
        assertEquals(0, userUserRepository.findAll().count())
    }



//    @Test
//    fun `test getAll`() = runBlocking {
//        val maxResults: Int = 100
//        val page: Int = 0
//        userUserDatabaseHandler.getAll(maxResults, page)
//        Unit
//    }

//    @Test
//    fun `test create`() = runBlocking {
//        val body: UserUser = UserUser()
//        userUserDatabaseHandler.create(body)
//        Unit
//    }

//    @Test
//    fun `test remove`() = runBlocking {
//        val id: Long = 0
//        userUserDatabaseHandler.remove(id)
//        Unit
//    }

//    @Test
//    fun `test update`() = runBlocking {
//        val body: UserUser = UserUser()
//        val id: Long = 0
//        userUserDatabaseHandler.update(body, id)
//        Unit
//    }
}
