package ffufm.kenneth.api.spec.handler.user.task.integration

import com.fasterxml.jackson.databind.ObjectMapper
import ffufm.kenneth.api.PassTestBase
import ffufm.kenneth.api.spec.handler.user.utils.EntityGenerator
import org.junit.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.MediaType
import org.springframework.security.test.context.support.WithMockUser
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.delete
import org.springframework.test.web.servlet.post
import org.springframework.test.web.servlet.put

class TaskTaskHandlerTest: PassTestBase() {

    @Autowired
    private lateinit var objectMapper: ObjectMapper

    @Autowired
    private lateinit var mockMvc: MockMvc

    @Test
    @WithMockUser
    fun `createTask should return isOk`() {

        val owner = userUserRepository.save(EntityGenerator.createUser())
        val project = projectProjectRepository.save(
            EntityGenerator.createProject().copy(owner = owner)
        )

        val body = EntityGenerator.createTasks().copy(
            assignee = owner,
            works = project )

        mockMvc.post(
            "/tasks/{userId}/{projectId}/",
            project.id!!, owner.id!!) {
            accept(MediaType.APPLICATION_JSON)
            contentType = MediaType.APPLICATION_JSON
            content = objectMapper.writeValueAsString(body)
        }.asyncDispatch().andExpect {
            status { isOk() }
        }
    }

    @Test
    @WithMockUser
    fun `createTask should return  invalid userId`() {

        val owner = userUserRepository.save(EntityGenerator.createUser())
        val project = projectProjectRepository.save(
            EntityGenerator.createProject().copy(owner = owner)
        )

        val invalidId : Long  = 123

        val body = EntityGenerator.createTasks().copy(
            assignee = owner,
            works = project )

        mockMvc.post(
            "/tasks/{userId}/{projectId}/",
            project.id!!, invalidId) {
            accept(MediaType.APPLICATION_JSON)
            contentType = MediaType.APPLICATION_JSON
            content = objectMapper.writeValueAsString(body)
        }.asyncDispatch().andExpect {
            status { isNotFound() }
        }
    }

    @Test
    @WithMockUser
    fun `update should return isOk`() {

        val owner = userUserRepository.save(EntityGenerator.createUser())
        val project = projectProjectRepository.save(
            EntityGenerator.createProject().copy( owner = owner ))

        val task = taskTaskRepository.save(EntityGenerator.createTasks().copy(
            assignee = owner,
            works = project ))

        val body = task.copy(
            status = "COMPLETED"
        )

        val invalidId : Long  = 123

        mockMvc.put(
            "/tasks/{id}/", task.id!!  ) {
            accept(MediaType.APPLICATION_JSON)
            contentType = MediaType.APPLICATION_JSON
            content = objectMapper.writeValueAsString(body)
        }.asyncDispatch().andExpect {
            status { isOk() }
        }
    }

    @Test
    @WithMockUser
    fun `remove should return isOk`() {

        val owner = userUserRepository.save(EntityGenerator.createUser())
        val project = projectProjectRepository.save(
            EntityGenerator.createProject().copy( owner = owner ))

        val task = taskTaskRepository.save(EntityGenerator.createTasks().copy(
            assignee = owner,
            works = project
        ))
        mockMvc.delete("/tasks/{id}/", task.id) {
            accept(MediaType.APPLICATION_JSON)
            contentType = MediaType.APPLICATION_JSON
        }.asyncDispatch().andExpect {
            status { isOk() }

        }
    }

}