package ffufm.kenneth.api.spec.handler.user.project.integration

import com.fasterxml.jackson.databind.ObjectMapper
import ffufm.kenneth.api.PassTestBase
import ffufm.kenneth.api.spec.handler.user.utils.EntityGenerator
import org.junit.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.MediaType
import org.springframework.security.test.context.support.WithMockUser
import org.springframework.test.web.servlet.*

class ProjectProjectHandlerTest: PassTestBase() {

    @Autowired
    private lateinit var objectMapper: ObjectMapper

    @Autowired
    private lateinit var mockMvc: MockMvc

    @Test
    @WithMockUser
    fun `get all projects should return correct pagination`(){
        mockMvc.get("/projects/"){
            accept(MediaType.APPLICATION_JSON)
            contentType = MediaType.APPLICATION_JSON
        }.andExpect {
            status {
                isOk()
            }
        }
    }


    @Test
    @WithMockUser
    fun `create project should return isOk`(){
        val body = EntityGenerator.createProject()
        mockMvc.post("/projects/") {
            accept(MediaType.APPLICATION_JSON)
            contentType = MediaType.APPLICATION_JSON
            content = objectMapper.writeValueAsString(body)
        }.andExpect {
            status { isOk() }
        }
    }

    @Test
    @WithMockUser
    fun `update project should return isOk`() {
        val project = projectProjectRepository.save(
            EntityGenerator.createProject()
        )
        val updatedProject = project.copy(
            name = "Olio",
            description = "Food sharing application",
            status = "DONE"
        )
        mockMvc.put("/projects/{id}/", project.id!!) {
            accept(MediaType.APPLICATION_JSON)
            contentType = MediaType.APPLICATION_JSON
            content = objectMapper.writeValueAsString(updatedProject)
        }.andExpect {
            status { isOk() }
        }
    }

    @Test
    @WithMockUser
    fun `delete project should return isOk`() {
        val project = projectProjectRepository.save(EntityGenerator.createProject())
        mockMvc.delete("/projects/{id}/", project.id!!) {
            accept(MediaType.APPLICATION_JSON)
            contentType = MediaType.APPLICATION_JSON
        }.andExpect {
            status { isOk() }
        }
    }
}