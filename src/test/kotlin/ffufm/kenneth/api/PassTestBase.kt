package ffufm.kenneth.api

import de.ffuf.pass.common.security.SpringContext
import de.ffuf.pass.common.security.SpringSecurityAuditorAware
import ffufm.kenneth.api.repositories.user.UserAddressRepository
import ffufm.kenneth.api.repositories.user.UserContactDetailRepository
import ffufm.kenneth.api.repositories.user.UserUserRepository
import ffufm.kenneth.api.repositories.user.project.ProjectprojectRepository
import ffufm.kenneth.api.repositories.user.task.TasktaskRepository
import org.junit.After
import org.junit.Before
import org.junit.runner.RunWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.context.ApplicationContext
import org.springframework.test.context.ActiveProfiles
import org.springframework.test.context.junit4.SpringRunner

@RunWith(SpringRunner::class)
@ActiveProfiles("test")
@SpringBootTest(classes = [SBKenneth::class, SpringSecurityAuditorAware::class])
@AutoConfigureMockMvc
abstract class PassTestBase {

    @Autowired
    lateinit var userUserRepository: UserUserRepository

    @Autowired
    lateinit var userContactDetailRepository: UserContactDetailRepository

    @Autowired
    lateinit var userAddressRepository: UserAddressRepository

    @Autowired
    lateinit var projectProjectRepository: ProjectprojectRepository

    @Autowired
    lateinit var taskTaskRepository: TasktaskRepository

    @Autowired
    lateinit var context: ApplicationContext

    @Before
    fun initializeContext() {
        SpringContext.context = context
    }

    @After
    fun cleanRepositories() {
        taskTaskRepository.deleteAll()
        projectProjectRepository.deleteAll()
        userAddressRepository.deleteAll()
        userContactDetailRepository.deleteAll()
        userUserRepository.deleteAll()
    }


}
