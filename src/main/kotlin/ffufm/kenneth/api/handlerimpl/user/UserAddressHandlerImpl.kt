package ffufm.kenneth.api.handlerimpl.user

import de.ffuf.pass.common.handlers.PassDatabaseHandler
import de.ffuf.pass.common.utilities.extensions.orElseThrow404
import ffufm.kenneth.api.repositories.user.UserAddressRepository
import ffufm.kenneth.api.spec.dbo.user.UserAddress
import ffufm.kenneth.api.spec.dbo.user.UserAddressDTO
import ffufm.kenneth.api.spec.handler.user.UserAddressDatabaseHandler
import kotlin.Long
import org.springframework.data.domain.Pageable
import org.springframework.data.repository.findByIdOrNull
import org.springframework.stereotype.Component

@Component("user.UserAddressHandler")
class UserAddressHandlerImpl : PassDatabaseHandler<UserAddress, UserAddressRepository>(),
        UserAddressDatabaseHandler {
    /**
     * : 
     * HTTP Code 200: Successfully deleted address
     */
    override suspend fun remove(id: Long) {
        return repository.deleteAll()
    }

    /**
     * : 
     * HTTP Code 200: The updated model
     * HTTP Code 404: The requested object could not be found by the submitted id.
     * HTTP Code 422: On or many fields contains a invalid value.
     */
    override suspend fun updateAddress(body: UserAddressDTO, id: Long): UserAddressDTO {
        val original = repository.findById(id).orElseThrow404(id)
        return repository.save(original).toDto()
    }

    /**
     * : 
     * HTTP Code 200: The created address
     */
    override suspend fun createAddress(body: UserAddressDTO, id: Long): UserAddressDTO {
        return repository.save(body.toEntity()).toDto()
    }
}
