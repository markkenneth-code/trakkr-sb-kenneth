package ffufm.kenneth.api.handlerimpl.user

import de.ffuf.pass.common.handlers.PassDatabaseHandler
import de.ffuf.pass.common.utilities.extensions.orElseThrow404
import ffufm.kenneth.api.repositories.user.UserContactDetailRepository
import ffufm.kenneth.api.repositories.user.UserUserRepository
import ffufm.kenneth.api.spec.dbo.user.UserContactDetail
import ffufm.kenneth.api.spec.dbo.user.UserContactDetailDTO
import ffufm.kenneth.api.spec.handler.user.UserContactDetailDatabaseHandler
import ffufm.kenneth.api.utils.Constants
import org.springframework.beans.factory.annotation.Autowired
import kotlin.Long
import org.springframework.http.HttpStatus
import org.springframework.stereotype.Component
import org.springframework.web.server.ResponseStatusException

@Component("user.UserContactDetailHandler")
class UserContactDetailHandlerImpl : PassDatabaseHandler<UserContactDetail,
        UserContactDetailRepository>(), UserContactDetailDatabaseHandler {


    @Autowired
    private lateinit var userRepository: UserUserRepository

    @Autowired
    private lateinit var contactRepository: UserContactDetailRepository

    @Autowired
    private lateinit var contactServiceImpl: UserContactDetailHandlerImpl

    /**
     * : 
     * HTTP Code 200: Successfully added contact detail
     */
    override suspend fun createContactDetail(body: UserContactDetailDTO, id: Long): UserContactDetailDTO {
        val bodyEntity = body.toEntity()
        val user = userRepository.findById(id).orElseThrow{
            ResponseStatusException(HttpStatus.NOT_FOUND, "User with id: $id does not exist")
        }
        val contactCount = repository.getContactDetailsByUserId(id)
        if(contactCount >= Constants.MAX_CONTACT_DETAILS){
            throw ResponseStatusException(HttpStatus.BAD_REQUEST, "You cannot create more than four contact details")
        }

        return repository.save(bodyEntity).toDto()
    }

    /**
     * : 
     * HTTP Code 200: Successfully deleted contact detail
     */
    override suspend fun remove(id: Long) {
        val original = repository.findById(id).orElseThrow404(id)
        return repository.delete(original)
    }

    /**
     * : 
     * HTTP Code 200: The updated model
     * HTTP Code 404: The requested object could not be found by the submitted id.
     * HTTP Code 422: On or many fields contains a invalid value.
     */
    override suspend fun updateContactDetails(body: UserContactDetailDTO, id: Long):
            UserContactDetailDTO {
        val original = repository.findById(id).orElseThrow404(id)
        return repository.save(original).toDto()
    }
}
