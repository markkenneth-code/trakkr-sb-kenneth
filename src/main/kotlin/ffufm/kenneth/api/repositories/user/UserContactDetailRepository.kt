package ffufm.kenneth.api.repositories.user

import de.ffuf.pass.common.repositories.PassRepository
import ffufm.kenneth.api.spec.dbo.user.UserContactDetail
import kotlin.Long
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.data.jpa.repository.Query
import org.springframework.stereotype.Repository

@Repository
interface UserContactDetailRepository : PassRepository<UserContactDetail, Long> {
    @Query(
        "SELECT t from UserContactDetail t LEFT JOIN FETCH t.user",
        countQuery = "SELECT count(id) FROM UserContactDetail"
    )
    fun findAllAndFetchUser(pageable: Pageable): Page<UserContactDetail>

    @Query(
        """
           SELECT CASE WHEN COUNT(cd) > 0 THEN TRUE ELSE FALSE END
           FROM UserContactDetail cd WHERE cd.contactDetail = :contactDetail
        """
    )
    fun doesContactDetailExist(contactDetail: String): Boolean

    @Query(
        """
            SELECT COUNT(cd) FROM UserContactDetail cd WHERE cd.user.id = :userId
        """
    )
    fun getContactDetailsByUserId(userId: Long): Int

}
