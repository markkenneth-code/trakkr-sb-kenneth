package ffufm.kenneth.api.repositories.user.project

import de.ffuf.pass.common.handlers.PassDatabaseHandler
import de.ffuf.pass.common.utilities.extensions.orElseThrow404
import de.ffuf.pass.common.utilities.extensions.toDtos
import ffufm.kenneth.api.repositories.user.UserUserRepository
import ffufm.kenneth.api.spec.dbo.project.ProjectProject
import ffufm.kenneth.api.spec.dbo.project.ProjectProjectDTO
import ffufm.kenneth.api.spec.handler.project.ProjectProjectDatabaseHandler
import org.springframework.data.domain.Page
import org.springframework.data.domain.PageRequest
import org.springframework.http.HttpStatus
import org.springframework.stereotype.Component
import org.springframework.web.server.ResponseStatusException

@Component("project.ProjectProjectHandler")
class ProjectProjectHandlerImpl(private val userRepository: UserUserRepository)
    : PassDatabaseHandler<ProjectProject, ProjectprojectRepository>(),
ProjectProjectDatabaseHandler{

    override suspend fun createProject(body: ProjectProjectDTO, id: Long): ProjectProjectDTO {

        val bodyEntity = body.toEntity()
        val owner = userRepository.findById(id).orElseThrow404(id)

        return repository.save(bodyEntity.copy( owner = owner )).toDto()
    }

    override suspend fun getAll(maxResults: Int, page: Int): Page<ProjectProjectDTO> {
        val pagination = PageRequest.of(page ?:0 ,maxResults ?: 100)
        return repository.findAll(pagination).toDtos()
    }

    override suspend fun update(body: ProjectProjectDTO, id: Long): ProjectProjectDTO {
        val project = repository.findById(id).orElseThrow{
            ResponseStatusException(
                HttpStatus.NOT_FOUND, "Project with id: $id does not exist"
            )
        }

        return repository.save(project.copy(
            name = body.name!!,
            description = body.description!!,
            status = body.status!!
        )).toDto()
    }

    override suspend fun remove(id: Long) {
        val project = repository.findById(id).orElseThrow{
            ResponseStatusException(
                HttpStatus.NOT_FOUND, "Project with id: $id does not exist"
            )
        }
        return repository.delete(project)
    }




}