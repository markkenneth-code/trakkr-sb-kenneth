package ffufm.kenneth.api.repositories.user.task

import de.ffuf.pass.common.repositories.PassRepository
import ffufm.kenneth.api.spec.dbo.task.TaskTask

interface TasktaskRepository:PassRepository<TaskTask, Long>