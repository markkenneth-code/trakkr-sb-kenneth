package ffufm.kenneth.api.repositories.user.task

import de.ffuf.pass.common.handlers.PassDatabaseHandler
import de.ffuf.pass.common.utilities.extensions.orElseThrow404
import de.ffuf.pass.common.utilities.extensions.toDtos
import ffufm.kenneth.api.repositories.user.UserUserRepository
import ffufm.kenneth.api.repositories.user.project.ProjectprojectRepository
import ffufm.kenneth.api.spec.dbo.task.TaskTask
import ffufm.kenneth.api.spec.dbo.task.TaskTaskDTO
import ffufm.kenneth.api.spec.handler.task.TaskTaskDatabaseHandler
import org.springframework.data.domain.Page
import org.springframework.data.domain.PageRequest
import org.springframework.http.HttpStatus
import org.springframework.stereotype.Component
import org.springframework.web.server.ResponseStatusException

@Component("task.TaskTaskHandler")
class TaskTaskHandlerImpl(
        private val userRepository: UserUserRepository,
        private val projectRepository: ProjectprojectRepository
): PassDatabaseHandler<TaskTask, TasktaskRepository>(),
        TaskTaskDatabaseHandler{

    override suspend fun createTasks(body: TaskTaskDTO, projectId: Long, userId: Long): TaskTaskDTO {
        val bodyEntity = body.toEntity()
        val assignee = userRepository.findById(userId).orElseThrow404(userId)
        val works = projectRepository.findById(projectId).orElseThrow404(projectId)

        return repository.save(bodyEntity.copy(
            assignee = assignee,
            works = works )).toDto()
    }

    override suspend fun getAll(maxResults: Int, page: Int): Page<TaskTaskDTO> {
        val pagination = PageRequest.of(page?: 0, maxResults ?: 100)
        return repository.findAll(pagination).toDtos()
    }

    override suspend fun remove(id: Long) {
        val task = repository.findById(id).orElseThrow {
           ResponseStatusException(
               HttpStatus.NOT_FOUND, "Task with id $id does not exist"
           )
        }
        return repository.delete(task)
    }

    override suspend fun update(body: TaskTaskDTO, id: Long): TaskTaskDTO {
        val task = repository.findById(id).orElseThrow {
            ResponseStatusException(
                HttpStatus.NOT_FOUND, "Task with id $id does not exist"
            )
        }
        return repository.save(task.copy(
            name = body.name!!,
            description = body.description!!,
            status = body.status!!
        )).toDto()
    }

}