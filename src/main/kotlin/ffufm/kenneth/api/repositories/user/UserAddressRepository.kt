package ffufm.kenneth.api.repositories.user

import de.ffuf.pass.common.repositories.PassRepository
import ffufm.kenneth.api.spec.dbo.user.UserAddress
import kotlin.Long
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.data.jpa.repository.Query
import org.springframework.stereotype.Repository

@Repository
interface UserAddressRepository : PassRepository<UserAddress, Long> {
    @Query(
        "SELECT t from UserAddress t LEFT JOIN FETCH t.user",
        countQuery = "SELECT count(id) FROM UserAddress"
    )
    fun findAllAndFetchUser(pageable: Pageable): Page<UserAddress>
}
