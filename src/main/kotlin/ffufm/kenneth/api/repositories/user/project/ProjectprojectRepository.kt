package ffufm.kenneth.api.repositories.user.project

import de.ffuf.pass.common.repositories.PassRepository
import ffufm.kenneth.api.spec.dbo.project.ProjectProject
import org.springframework.stereotype.Repository

@Repository
interface ProjectprojectRepository:PassRepository<ProjectProject, Long>