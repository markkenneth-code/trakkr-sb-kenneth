package ffufm.kenneth.api.spec.dbo.task

import am.ik.yavi.builder.ValidatorBuilder
import de.ffuf.pass.common.models.PassDTO
import de.ffuf.pass.common.models.PassDTOModel
import de.ffuf.pass.common.models.PassDtoSerializer
import de.ffuf.pass.common.models.PassModelValidation
import de.ffuf.pass.common.models.idDto
import ffufm.kenneth.api.spec.dbo.project.ProjectProject
import ffufm.kenneth.api.spec.dbo.project.ProjectProjectDTO
import ffufm.kenneth.api.spec.dbo.user.UserUser
import ffufm.kenneth.api.spec.dbo.user.UserUserDTO
import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.FetchType
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id
import javax.persistence.JoinColumn
import javax.persistence.Lob
import javax.persistence.ManyToOne
import javax.persistence.Table
import kotlin.Long
import kotlin.String
import kotlin.reflect.KClass
import org.springframework.stereotype.Component
import org.springframework.stereotype.Service

@Entity(name = "TaskTask")
@Table(name = "task_task")
data class TaskTask(
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    var id: Long? = null,
    /**
     * Sample: Company a task
     */
    @Column(
        nullable = false,
        updatable = true,
        name = "name"
    )
    @Lob
    val name: String = "",
    /**
     * Description of the task
     * Sample: for the new task
     */
    @Column(
        nullable = false,
        updatable = true,
        name = "description"
    )
    @Lob
    val description: String = "",
    /**
     * Status of the task
     * Sample: ON-GOING
     */
    @Column(
        nullable = false,
        updatable = true,
        name = "status"
    )
    @Lob
    val status: String = "",
    @ManyToOne(fetch = FetchType.LAZY, optional = true)
    @JoinColumn(nullable = true)
    val assignee: UserUser? = null,
    @ManyToOne(fetch = FetchType.LAZY, optional = true)
    @JoinColumn(nullable = true)
    val works: ProjectProject? = null
) : PassDTOModel<TaskTask, TaskTaskDTO, Long>() {
    override fun toDto(): TaskTaskDTO = super.toDtoInternal(TaskTaskSerializer::class as
            KClass<PassDtoSerializer<PassDTOModel<TaskTask, TaskTaskDTO, Long>, TaskTaskDTO, Long>>)

    override fun readId(): Long? = this.id

    override fun toString(): String = super.toString()
}

data class TaskTaskDTO(
    val id: Long? = null,
    /**
     * Sample: Company a task
     */
    val name: String? = "",
    /**
     * Description of the task
     * Sample: for the new task
     */
    val description: String? = "",
    /**
     * Status of the task
     * Sample: ON-GOING
     */
    val status: String? = "",
    val assignee: UserUserDTO? = null,
    val works: ProjectProjectDTO? = null
) : PassDTO<TaskTask, Long>() {
    override fun toEntity(): TaskTask = super.toEntityInternal(TaskTaskSerializer::class as
            KClass<PassDtoSerializer<PassDTOModel<TaskTask, PassDTO<TaskTask, Long>, Long>,
            PassDTO<TaskTask, Long>, Long>>)

    override fun readId(): Long? = this.id
}

@Component
class TaskTaskSerializer : PassDtoSerializer<TaskTask, TaskTaskDTO, Long>() {
    override fun toDto(entity: TaskTask): TaskTaskDTO = cycle(entity) {
        TaskTaskDTO(
                id = entity.id,
        name = entity.name,
        description = entity.description,
        status = entity.status,
        assignee = entity.assignee?.idDto() ?: entity.assignee?.toDto(),
        works = entity.works?.idDto() ?: entity.works?.toDto()
                )}

    override fun toEntity(dto: TaskTaskDTO): TaskTask = TaskTask(
            id = dto.id,
    name = dto.name ?: "",
    description = dto.description ?: "",
    status = dto.status ?: "",
    assignee = dto.assignee?.toEntity(),
    works = dto.works?.toEntity()
            )
    override fun idDto(id: Long): TaskTaskDTO = TaskTaskDTO(
            id = id,
    name = null,
    description = null,
    status = null,

            )}

@Service("task.TaskTaskValidator")
class TaskTaskValidator : PassModelValidation<TaskTask> {
    override fun buildValidator(validatorBuilder: ValidatorBuilder<TaskTask>):
            ValidatorBuilder<TaskTask> = validatorBuilder.apply {
    }
}

@Service("task.TaskTaskDTOValidator")
class TaskTaskDTOValidator : PassModelValidation<TaskTaskDTO> {
    override fun buildValidator(validatorBuilder: ValidatorBuilder<TaskTaskDTO>):
            ValidatorBuilder<TaskTaskDTO> = validatorBuilder.apply {
    }
}
