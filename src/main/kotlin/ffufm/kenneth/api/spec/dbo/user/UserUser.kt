package ffufm.kenneth.api.spec.dbo.user

import am.ik.yavi.builder.ValidatorBuilder
import am.ik.yavi.builder.konstraint
import am.ik.yavi.builder.konstraintOnObject
import de.ffuf.pass.common.models.PassDTO
import de.ffuf.pass.common.models.PassDTOModel
import de.ffuf.pass.common.models.PassDtoSerializer
import de.ffuf.pass.common.models.PassModelValidation
import de.ffuf.pass.common.models.idDto
import de.ffuf.pass.common.security.SpringContext
import de.ffuf.pass.common.utilities.extensions.konstraint
import de.ffuf.pass.common.utilities.extensions.toEntities
import de.ffuf.pass.common.utilities.extensions.toSafeDtos
import ffufm.kenneth.api.spec.dbo.project.ProjectProject
import ffufm.kenneth.api.spec.dbo.project.ProjectProjectDTO
import ffufm.kenneth.api.spec.dbo.project.ProjectProjectSerializer
import ffufm.kenneth.api.spec.dbo.task.TaskTaskSerializer
import ffufm.kenneth.api.spec.dbo.user.UserAddressSerializer
import ffufm.kenneth.api.spec.dbo.user.UserContactDetailSerializer
import java.util.TreeSet
import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.FetchType
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id
import javax.persistence.Index
import javax.persistence.Lob
import javax.persistence.OneToMany
import javax.persistence.SequenceGenerator
import javax.persistence.Table
import javax.persistence.UniqueConstraint
import kotlin.Boolean
import kotlin.Long
import kotlin.String
import kotlin.collections.List
import kotlin.reflect.KClass
import org.hibernate.annotations.CacheConcurrencyStrategy
import org.hibernate.annotations.ColumnDefault
import org.hibernate.annotations.FetchMode
import org.springframework.beans.factory.getBeansOfType
import org.springframework.stereotype.Component
import org.springframework.stereotype.Service

/**
 * User documentation
 */
@Entity(name = "UserUser")
@Table(name = "user_user")
data class UserUser(
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    var id: Long? = null,
    /**
     * firstName of the user
     * Sample: Kenneth
     */
    @Column(
        length = 50,
        updatable = true,
        nullable = false,
        name = "first_name"
    )
    val firstName: String = "",
    /**
     * lastName of the user
     * Sample: Dela Cruz
     */
    @Column(
        length = 50,
        updatable = true,
        nullable = false,
        name = "last_name"
    )
    val lastName: String = "",
    /**
     * Email of the user
     * Sample: kennethigop@gmail.com
     */
    @Column(
        nullable = false,
        updatable = true,
        name = "email"
    )
    @Lob
    val email: String = "",
    /**
     * if the user is still active
     * Sample: TRUE
     */
    @ColumnDefault("TRUE")
    @Column(name = "is_active")
    val isActive: Boolean = false,
    /**
     * Type of user: GC = General Contractor, SC = Sub Contractor, CR = Contractor
     * Sample: GC
     */
    @Column(
        length = 2,
        updatable = true,
        nullable = false,
        name = "user_type"
    )
    val userType: String = "",
    @OneToMany(mappedBy = "owner", fetch = FetchType.LAZY)
    val projects: List<ProjectProject>? = mutableListOf()
) : PassDTOModel<UserUser, UserUserDTO, Long>() {
    override fun toDto(): UserUserDTO = super.toDtoInternal(UserUserSerializer::class as
            KClass<PassDtoSerializer<PassDTOModel<UserUser, UserUserDTO, Long>, UserUserDTO, Long>>)

    override fun readId(): Long? = this.id

    override fun toString(): String = super.toString()

    enum class Usertype(
        val value: String
    ) {
        CONTRACTOR("CR"),

        GENERAL_CONTRACTOR("GC"),

        SUBCONTRACTOR("SC");
    }
}

/**
 * User documentation
 */
data class UserUserDTO(
    val id: Long? = null,
    /**
     * firstName of the user
     * Sample: Kenneth
     */
    val firstName: String? = "",
    /**
     * lastName of the user
     * Sample: Dela Cruz
     */
    val lastName: String? = "",
    /**
     * Email of the user
     * Sample: kennethigop@gmail.com
     */
    val email: String? = "",
    /**
     * if the user is still active
     * Sample: TRUE
     */
    val isActive: Boolean? = false,
    /**
     * Type of user: GC = General Contractor, SC = Sub Contractor, CR = Contractor
     * Sample: GC
     */
    val userType: String? = "",
    val projects: List<ProjectProjectDTO>? = null
) : PassDTO<UserUser, Long>() {
    override fun toEntity(): UserUser = super.toEntityInternal(UserUserSerializer::class as
            KClass<PassDtoSerializer<PassDTOModel<UserUser, PassDTO<UserUser, Long>, Long>,
            PassDTO<UserUser, Long>, Long>>)

    override fun readId(): Long? = this.id
}

@Component
class UserUserSerializer : PassDtoSerializer<UserUser, UserUserDTO, Long>() {
    override fun toDto(entity: UserUser): UserUserDTO = cycle(entity) {
        UserUserDTO(
                id = entity.id,
        firstName = entity.firstName,
        lastName = entity.lastName,
        email = entity.email,
        isActive = entity.isActive,
        userType = entity.userType,
        projects = entity.projects?.toSafeDtos()
                )}

    override fun toEntity(dto: UserUserDTO): UserUser = UserUser(
            id = dto.id,
    firstName = dto.firstName ?: "",
    lastName = dto.lastName ?: "",
    email = dto.email ?: "",
    isActive = dto.isActive ?: false,
    userType = dto.userType ?: "",
    projects = dto.projects?.toEntities() ?: emptyList()
            )
    override fun idDto(id: Long): UserUserDTO = UserUserDTO(
            id = id,
    firstName = null,
    lastName = null,
    email = null,
    isActive = null,
    userType = null,

            )}

@Service("user.UserUserValidator")
class UserUserValidator : PassModelValidation<UserUser> {
    override fun buildValidator(validatorBuilder: ValidatorBuilder<UserUser>):
            ValidatorBuilder<UserUser> = validatorBuilder.apply {
        konstraint(UserUser::firstName) {
            notNull()
            @Suppress("MagicNumber")
            lessThanOrEqual(50)
        }
        konstraint(UserUser::lastName) {
            notNull()
            @Suppress("MagicNumber")
            lessThanOrEqual(50)
        }
        konstraint(UserUser::userType) {
            notNull()
            @Suppress("MagicNumber")
            lessThanOrEqual(2)
        }
    }
}

@Service("user.UserUserDTOValidator")
class UserUserDTOValidator : PassModelValidation<UserUserDTO> {
    override fun buildValidator(validatorBuilder: ValidatorBuilder<UserUserDTO>):
            ValidatorBuilder<UserUserDTO> = validatorBuilder.apply {
        konstraint(UserUserDTO::firstName) {
            notNull()
            @Suppress("MagicNumber")
            lessThanOrEqual(50)
        }
        konstraint(UserUserDTO::lastName) {
            notNull()
            @Suppress("MagicNumber")
            lessThanOrEqual(50)
        }
        konstraint(UserUserDTO::userType) {
            notNull()
            @Suppress("MagicNumber")
            lessThanOrEqual(2)
        }
    }
}
