package ffufm.kenneth.api.spec.handler.user

import com.fasterxml.jackson.module.kotlin.readValue
import de.ffuf.pass.common.handlers.PassMvcHandler
import ffufm.kenneth.api.spec.dbo.user.UserAddressDTO
import kotlin.Long
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestMethod
import org.springframework.web.multipart.MultipartFile
import org.springframework.web.multipart.MultipartHttpServletRequest
import org.springframework.web.server.ResponseStatusException

interface UserAddressDatabaseHandler {
    /**
     * : 
     * HTTP Code 200: The created address
     */
    suspend fun createAddress(body: UserAddressDTO, id: Long): UserAddressDTO

    /**
     * : 
     * HTTP Code 200: Successfully deleted address
     */
    suspend fun remove(id: Long)

    /**
     * : 
     * HTTP Code 200: The updated model
     * HTTP Code 404: The requested object could not be found by the submitted id.
     * HTTP Code 422: On or many fields contains a invalid value.
     */
    suspend fun updateAddress(body: UserAddressDTO, id: Long): UserAddressDTO
}

@Controller("user.Address")
class UserAddressHandler : PassMvcHandler() {
    @Autowired
    lateinit var databaseHandler: UserAddressDatabaseHandler

    /**
     * : 
     * HTTP Code 200: The created address
     */
    @RequestMapping(value = ["/users/{id:\\d+}/address/"], method = [RequestMethod.POST])
    suspend fun createAddress(@RequestBody body: UserAddressDTO, @PathVariable("id") id: Long):
            ResponseEntity<*> {
        body.validateOrThrow()
        return success { databaseHandler.createAddress(body, id) }
    }

    /**
     * : 
     * HTTP Code 200: Successfully deleted address
     */
    @RequestMapping(value = ["/users/address/{id:\\d+}/"], method = [RequestMethod.DELETE])
    suspend fun remove(@PathVariable("id") id: Long): ResponseEntity<*> {

        return success { databaseHandler.remove(id) }
    }

    /**
     * : 
     * HTTP Code 200: The updated model
     * HTTP Code 404: The requested object could not be found by the submitted id.
     * HTTP Code 422: On or many fields contains a invalid value.
     */
    @RequestMapping(value = ["/users/address/{id:\\d+}/"], method = [RequestMethod.PUT])
    suspend fun updateAddress(@RequestBody body: UserAddressDTO, @PathVariable("id") id: Long):
            ResponseEntity<*> {
        body.validateOrThrow()
        return success { databaseHandler.updateAddress(body, id) }
    }
}
