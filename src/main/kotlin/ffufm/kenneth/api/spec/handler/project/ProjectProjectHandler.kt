package ffufm.kenneth.api.spec.handler.project

import de.ffuf.pass.common.handlers.PassMvcHandler
import ffufm.kenneth.api.spec.dbo.project.ProjectProjectDTO
import kotlin.Int
import kotlin.Long
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.domain.Page
import org.springframework.http.ResponseEntity
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestMethod
import org.springframework.web.bind.annotation.RequestParam

interface ProjectProjectDatabaseHandler {
    /**
     * : 
     * HTTP Code 201: The created project
     */
    suspend fun createProject(body: ProjectProjectDTO, id: Long): ProjectProjectDTO

    /**
     * Get all Projects: Returns all Projects from the system that the user has access to.
     * HTTP Code 200: List of Projects
     */
    suspend fun getAll(maxResults: Int = 100, page: Int = 0): Page<ProjectProjectDTO>

    /**
     * Delete Project by id.: Deletes one specific Project.
     */
    suspend fun remove(id: Long)

    /**
     * Update the Project: Updates an existing Project
     * HTTP Code 200: The updated model
     * HTTP Code 404: The requested object could not be found by the submitted id.
     * HTTP Code 422: On or many fields contains a invalid value.
     */
    suspend fun update(body: ProjectProjectDTO, id: Long): ProjectProjectDTO
}

@Controller("project.Project")
class ProjectProjectHandler : PassMvcHandler() {
    @Autowired
    lateinit var databaseHandler: ProjectProjectDatabaseHandler

    /**
     * : 
     * HTTP Code 201: The created project
     */
    @RequestMapping(value = ["/projects/{userId:\\d+}/"], method = [RequestMethod.POST])
    suspend fun createProject(@RequestBody body: ProjectProjectDTO, @PathVariable("userId")
            userId: Long): ResponseEntity<*> {
        body.validateOrThrow()
        return success { databaseHandler.createProject(body, userId) }
    }

    /**
     * Get all Projects: Returns all Projects from the system that the user has access to.
     * HTTP Code 200: List of Projects
     */
    @RequestMapping(value = ["/projects/"], method = [RequestMethod.GET])
    suspend fun getAll(@RequestParam("maxResults") maxResults: Int? = 100, @RequestParam("page")
            page: Int? = 0): ResponseEntity<*> {

        return paging { databaseHandler.getAll(maxResults ?: 100, page ?: 0) }
    }

    /**
     * Delete Project by id.: Deletes one specific Project.
     */
    @RequestMapping(value = ["/projects/{id:\\d+}/"], method = [RequestMethod.DELETE])
    suspend fun remove(@PathVariable("id") id: Long): ResponseEntity<*> {

        return success { databaseHandler.remove(id) }
    }

    /**
     * Update the Project: Updates an existing Project
     * HTTP Code 200: The updated model
     * HTTP Code 404: The requested object could not be found by the submitted id.
     * HTTP Code 422: On or many fields contains a invalid value.
     */
    @RequestMapping(value = ["/projects/{id:\\d+}/"], method = [RequestMethod.PUT])
    suspend fun update(@RequestBody body: ProjectProjectDTO, @PathVariable("id") id: Long):
            ResponseEntity<*> {
        body.validateOrThrow()
        return success { databaseHandler.update(body, id) }
    }
}
