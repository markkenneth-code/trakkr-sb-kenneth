package ffufm.kenneth.api.spec.handler.task

import de.ffuf.pass.common.handlers.PassMvcHandler
import ffufm.kenneth.api.spec.dbo.task.TaskTask
import ffufm.kenneth.api.spec.dbo.task.TaskTaskDTO
import kotlin.Int
import kotlin.Long
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.domain.Page
import org.springframework.http.ResponseEntity
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestMethod
import org.springframework.web.bind.annotation.RequestParam

interface TaskTaskDatabaseHandler {
    /**
     * : 
     * HTTP Code 201: The created task
     */
    suspend fun createTasks(
        body: TaskTaskDTO,
        projectId: Long,
        userId: Long
    ): TaskTaskDTO

    /**
     * Get all Tasks: Returns all Tasks from the system that the user has access to.
     * HTTP Code 200: List of Tasks
     */
    suspend fun getAll(maxResults: Int = 100, page: Int = 0): Page<TaskTaskDTO>

    /**
     * Delete Task by id.: Deletes one specific Task.
     */
    suspend fun remove(id: Long)

    /**
     * Update the Task: Updates an existing Task
     * HTTP Code 200: The updated model
     * HTTP Code 404: The requested object could not be found by the submitted id.
     * HTTP Code 422: On or many fields contains a invalid value.
     */
    suspend fun update(body: TaskTaskDTO, id: Long): TaskTaskDTO
}

@Controller("task.Task")
class TaskTaskHandler : PassMvcHandler() {
    @Autowired
    lateinit var databaseHandler: TaskTaskDatabaseHandler

    /**
     * : 
     * HTTP Code 201: The created task
     */
    @RequestMapping(value = ["/tasks/{userId:\\d+}/{projectId:\\d+}/"], method =
            [RequestMethod.POST])
    suspend fun createTasks(
        @RequestBody body: TaskTaskDTO,
        @PathVariable("projectId") projectId: Long,
        @PathVariable("userId") userId: Long
    ): ResponseEntity<*> {

        return success { databaseHandler.createTasks(body, projectId, userId) }
    }

    /**
     * Get all Tasks: Returns all Tasks from the system that the user has access to.
     * HTTP Code 200: List of Tasks
     */
    @RequestMapping(value = ["/tasks/"], method = [RequestMethod.GET])
    suspend fun getAll(@RequestParam("maxResults") maxResults: Int? = 100, @RequestParam("page")
            page: Int? = 0): ResponseEntity<*> {

        return paging { databaseHandler.getAll(maxResults ?: 100, page ?: 0) }
    }

    /**
     * Delete Task by id.: Deletes one specific Task.
     */
    @RequestMapping(value = ["/tasks/{id:\\d+}/"], method = [RequestMethod.DELETE])
    suspend fun remove(@PathVariable("id") id: Long): ResponseEntity<*> {

        return success { databaseHandler.remove(id) }
    }

    /**
     * Update the Task: Updates an existing Task
     * HTTP Code 200: The updated model
     * HTTP Code 404: The requested object could not be found by the submitted id.
     * HTTP Code 422: On or many fields contains a invalid value.
     */
    @RequestMapping(value = ["/tasks/{id:\\d+}/"], method = [RequestMethod.PUT])
    suspend fun update(@RequestBody body: TaskTaskDTO, @PathVariable("id") id: Long):
            ResponseEntity<*> {
        body.validateOrThrow()
        return success { databaseHandler.update(body, id) }
    }
}
